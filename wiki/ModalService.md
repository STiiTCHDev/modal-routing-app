**Description**

C'est un simple service angular qui se charge de fournir une méthode permettant d'appeler un composant de type [ModalComponent](ModalComponent). En interne, il ouvre une modal contenant le [ModalContainerComponent](ModalContainerComponent) en lui passant le composant enfant à ouvrir dans son body et les options passées.

Il retourne un objet contenant les callbacks de retour auxquelles on peut souscrire.

**Méthodes**

```typescript
ModalService.openModal(modal: ModalComponent, options: MatDialogConfig): ModalParentCallbacks
```

Voir: 
- [ModalComponent](ModalComponent)
- [ModalParentCallbacks](ModalParentCallbacks)

**Exemple d'utilisation**

Ici, l'appel ne change pas, on passe juste par ModalService au lieu de MatDialog.
Le changement, c'est qu'en retour, elle renvoi un objet contenant des observables pour chaque action, ici on souscrit à onClose et onSubmit.

```typescript
import { ModalService } from '@app/core/modal/services/modal.service';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent {
  
  constructor(private modalService: ModalService) {}

  openEnfantModal(someData: any = null) {
    const modal: ModalParentCallbacks = this.modalService.openModal(EnfantModalComponent, {
      data: someData,
      height: '500px',
      width: '500px',
    });

    modal.onClose.subscribe(() => {
      console.log('Modal closed');
    }

    modal.onSubmit.subscribe((result: any) => {
      console.log('Modal submitted');
      /* Plein de trucs métier */
    }
  }
}
```

Suite de la visite:
- [ModalComponent](ModalComponent)
