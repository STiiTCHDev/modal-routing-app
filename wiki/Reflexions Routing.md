Le routing dans l'appli ce serait le rêve au premier abord, tu sais, plus besoin de réouvrir 25 modals pour teste ta fonctionnalité, par exemple ;)

J'ai réussi à le faire fonctionner en passant par un Guard a placer sur le canActivate des routes de type modal. 

Cependant, la question du passage de données se pose, le système présenté jusqu'à présent nous offre une gestion personnalisée et précise des actions se produisant sur une modale et des retours possibles.

Mais via les routes ?

**Le pourquoi du comment:**

- [ ] Si on limite le passage de données (entre l'appelant et la modal appelée via une route) aux paramètres dans l'url, on va vite se retrouver bloqué. Impossible de passer un objet complexe de cette manière.
  - On ne peut pas se permettre de refaire une requete api pour chaque modal qu'on ouvre si la seule variable disponible est l'id par exemple "/contact/4568". Avec l'imbrication de modals, on va se retrouver avec des millions de requetes pour rien, les data sont déjà disponibles plus haut dans l'arborescence.
  - Une solution est d'utiliser les states des routes angular. Avec, on peut lors d'un routing, passer un objet complexe sans passer par l'url.
  - Un soucis persiste, lorsque l'on ferme une modal, il faudra mettre en place une mécanique de redirection afin que la route s'adapte à l'arborescence des modals. De plus, pour retourner des data, il faudra les passer via le state qui renvoit au parent. Résultat, un composant modal qui ouvre une modal recevrait 2 type de states différents .. celui d'ouverture et celui de fermeture de son enfant .. bref, si t'as rien compris au bout de cette phrase c'est normal, c'est une galère à concevoir xD en gros, gérer les données de retour c'est une galère.
  - Pour finir le casse tête, vu que les states sont en runtime, qu'est ce qui se passe si on reload ? Bah oui, plus aucune data de navigation :p Du coup, obligé de refaire des requetes ...

- [ ] Du coup, au lieu de passer sur du routing, il y a deux pistes que je veux creuser:
  - Déjà, la possibilité de persister notre routing de modals dans les localstorages via un service. L'avantage, c'est que je peux complètement imbriquer ça dans le nouveau système de modals, pour que ça se fasse tout seul. En gros, on stocke chaque modal qui s'ouvre et les data d'initialisation, au rechargement, on automatise une routine qui vérifie la navigation, et qui réouvre les modals et refait les requetes nécéssaires tout seul.
  - Ensuite, la possibilité de créer, sur le modèle des routes, une directive [modalLink], qui nous permettrait de configurer via l'html un appel de modal pour les cas simples qui ne nécéssitent pas de gestion du retour.


Oui je sais pardon cette partie là était plus "jetée sur le papier" que travaillée .. mais bon ^^
