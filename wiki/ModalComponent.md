**Description**

ModalComponent est le composant parent de tout composant destiné à être affiché dans une modal. Il établi la communication avec le [ModalContainerComponent](ModalContainerComponent), fournit des attributs pour gérer la modal depuis l'enfant et permettra de factoriser toute fonctionnement générique aux modals.

**Attributs & Méthodes**

```typescript
/* Objet de configuration permettant d'intéragir avec la modal */
public Modal: ModalConfig
/* Objet permettant de déclencher les actions de retour */
public Response: ModalResponse

/* Initialisation de base du composant, à appeler dans le ngOnInit de l'enfant ! */
public ngOnInit(): void 
/* Gestion de cdRef.detectChanges() pour qu'il l'applique à l'enfant et au container en même temps */
protected refresh(): void 
```

Voir: 
- [ModalConfig](ModalConfig)
- [ModalResponse](ModalResponse)

**Exemple d'utilisation**

Pour coder une modal, on crée un composant qui hérite de ModalComponent

```typescript
import { ModalComponent } from '@app/core/modal/components/modal.component';

@Component({
  selector: 'app-enfant-modal',
  templateUrl: './enfant-modal.component.html',
  styleUrls: ['./enfant-modal.component.scss']
})
export class EnfantModalComponent extends ModalComponent implements OnInit {

  /* ModalOptions est récupéré par ModalComponent à l'initialisation pour binder les options "par défaut" de l'enfant dans l'objet Modal */
  public ModalOptions: ModalOptions = {
    title: 'Créer quelque chose',
    icon: 'un-icon'
  }

  /* Idem que ModalOptions mais pour les actions */
  public ModalActions: ModalActions = {
    submit: {
      text: 'Créer'
    }
  }

  public form!: FormGroup;
  
  constructor(
    private formBuilder: FormBuilder
  ) {
    /* Instancie le ModalComponent */
    super();
  }

  ngOnInit() {
    /* Procède aux initialisations & bindings */
    super.ngOnInit();

    /* Code métier */
    this.form = this.formBuilder.group({
     // Champs du formulaire */
    });

    /* On regarde si on a des data passées par le parent via l'objet Modal, si oui, on patch le form */
    if (this.Modal.data) {
      this.form.patchValue(this.Modal.data);
      //On peut même utiliser l'objet Modal pour changer à la volée le container. 
      this.Modal.options.title = 'Editer quelque chose'; // Ici, changer de titre de modal
      this.Modal.actions.submit.text = 'Modifier'; //Là, changer le texte d'un bouton d'action
    }

    /* Et on pense à rafraichir pour être sûrs que tout sera bien actualisé */
    this.refresh();
  }

  /* Il ne reste plus qu'à définir des méthodes d'actions, le container se chargera d'afficher les boutons d'actions correspondants */

  public onClose(): void {
    /* On utilise l'objet Response fournit par le ModalComponent pour déclencher l'action */
    this.Response.close();
  }

  public onSubmit(): void {
    /* Tout plein de code métier ... */

    /* Les méthodes d'action de Response peuvent toutes, optionnellement, retourner un valeur en paramètre, même close. */
    this.Response.submit(this.form.value);
  }

}
```

Une fois cette base opérationnelle, m'est revenue un idée pour parvenir à faire passer nos modals par le routing. 
S'en est suivi des expériences et j'en arrive aux réflexions suivantes : [Reflexions Routing](Reflexions Routing)
