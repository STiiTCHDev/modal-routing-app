**Petite intro pour démarrer:**

Lorsque l'on utilise trop le service modal d'Angular-Material (suivez mon regard ...) on se retrouve avec une application dont les composant, ainsi que leur templates, se retrouvent surchargés par une quantité énorme de code structurel intégralement dédié à la mise en place de la navigation par modals.

Pire, tout ce code se trouve répété autant de fois qu'il y a d'imbrications de modals, avec:
- Dans le parent le code d'ouverture et le callback de retour (pour chaque enfant potentiel).
- Dans l'enfant toute la logique de récupération des data d'entrée et celle de fermeture avec envoi des data de retour. 
- Et cerise sur le gâteau, chaque template de modal qui redéfini quasiment le même header, le même footer et le code css qui va avec. 

#rage


**Mon analyse est donc la suivante:**

- [ ] Il faut factoriser la structure de la modal dans un **composant container** unique et générique:
  - ce qui permet de ne définir qu'une fois le header et le footer pour toute l'appli
  - ça implique un composant configurable en fonction des besoins
    - activer ou non le bouton supprimer
    - definir le titre, l'icon
    - changer le texte du bouton submit
    - etc...
  - ce sera lui qui sera chargé d'instancier le **composant enfant** appelé dans son body
- [ ] Il faut créer un service personnalisé pour encadrer l'utilisation des MatDialogs
  - permet de définir la manière donc on souhaite appeler un composant modal
  - il s'occupera d'ouvrir le composant modal générique (**container**) en lui passant en paramètre le **composant enfant** demandé
- [ ] Il faut standardiser nos composants destinés à être une modal (**enfant**):
  - permet de définir des comportements génériques dans le but d'interagir avec le **container**
  - normalise le code structurel de la modal, laissant plus de place au code métier.
  - nous offre l'opportunité de fournir certains fonctionnements génériques (helpers) par défaut: 
    - activer ou non la confirmation sur une action
    - récuperer automatiquement les data passées par le parent
    - renvoyer des data lors d'une action
    - voir, à terme, factoriser le fonctionnement des formulaires
    - etc...


**Petite précision lexicale:** ici, je parle d'actions, par action j'entends un event déclenché par l'utilisateur, qui, dans la structure détaillée ci-dessus, sera activé au niveau du container, mais devra déclencher une méthode de l'enfant et pourra éventuellement mener à une fermeture de la modal avec un passage de data vers le parent. Exemple: 
  - Close: Le bouton se trouve dans le header, doit éventuellement déclencher une méthode de l'enfant et fermer la modal.
  - Submit: même chose, mais dans le footer, déclenche une méthode de l'enfant et doit souvent passer des data en retour au parent en fermant.
  - Print: Dans le header, mais s'exécute dans l'enfant, sans forcément fermer la modal.


Ceci étant posé, attaquons par le service [ModalService](ModalService), le point d'entrée du système
