import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class UrlService {

  constructor(private location: Location) { }

  public changeUrl(url: string): void {
    this.location.go(url);
  }

}
