import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[modalLink]'
})
export class ModalLinkDirective {

  @Input() modalLink: string[] = [];

  constructor() { }

  @HostListener('click', ['$event'])
  onClick($event: any) {
    console.log('ModalLink click event');
  }

}
