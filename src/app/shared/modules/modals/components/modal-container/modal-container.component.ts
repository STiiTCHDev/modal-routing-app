import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ComponentFactoryResolver, ComponentRef, Inject, OnDestroy, Type, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ComponentType } from '@angular/cdk/portal';
import { Subject } from 'rxjs';
import { ModalComponent } from '../modal/modal.component';
import { ModalOptions, ModalActions } from '../modal/modal-config';

export interface ModalContainerComponent {
  [key: string]: any;
}

interface DialogData {
  componentRef: ComponentRef<ModalComponent>;
  type: ComponentType<ModalComponent>;
  data: {}
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal-container.component.html',
  styleUrls: ['./modal-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalContainerComponent implements AfterViewInit, OnDestroy {

  @ViewChild('body', {read: ViewContainerRef}) bodyComponent!: ViewContainerRef;
  public bodyComponentRef!: ComponentRef<ModalComponent>;
  public type: Type<ModalComponent>;
  public bodyComponentInstance: ModalComponent;

  public parentData: any = {};
  public bodyOptions!: ModalOptions;
  public bodyActions!: ModalActions;

  private onCloseAction: Subject<any> = new Subject();
  private onSubmitAction: Subject<any> = new Subject();

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private cdRef: ChangeDetectorRef,
    public dialogRef: MatDialogRef<Component>,
    @Inject(MAT_DIALOG_DATA) public options: DialogData) {
      /* Recuperation du ComponentType et du ComponentRef du ModalComponent */
      this.type = options.type;
      this.bodyComponentInstance = options.componentRef.instance;
      /* Recuperation des data du parent */
      this.parentData = options.data;

      /* On crée les actions */
      this.registerAction('close');
      this.registerAction('submit');
    }

  /**
   * Initialise le ModalComponent
   */
  ngAfterViewInit() {
     this.injectBodyComponent(this.type);

    /* On bind les data passées à l'appel dans le bodyComponent */
    if (this.parentData && Object.keys(this.parentData).length) {
      this.bodyComponentInstance.Modal.data = this.parentData;
    }

    /* On bind les options depuis le bodyComponent pour les utiliser dans le template du container */
    this.bodyOptions = this.bodyComponentInstance.Modal.options;
    this.bodyActions = this.bodyComponentInstance.Modal.actions;

    /* On bind les actions du container dans le bodyComponent */
    this.bodyComponentInstance.onCloseAction = this.onCloseAction;
    this.bodyComponentInstance.onSubmitAction = this.onSubmitAction;

    this.bodyComponentInstance.onRefresh.subscribe(() => {
      this.bodyOptions = this.bodyComponentInstance.Modal.options;
      this.bodyActions = this.bodyComponentInstance.Modal.actions;
      this.cdRef.detectChanges();
    });

    this.cdRef.detectChanges();
  }

  /**
   * Instancie le ModalComponent dans le body du ModalContainer
   */
  public injectBodyComponent(type: Type<ModalComponent>): void {
    const factory = this.componentFactoryResolver.resolveComponentFactory(type);
    this.bodyComponentRef = this.bodyComponent.createComponent(factory);
    this.bodyComponentInstance = this.bodyComponentRef.instance;
  }

  /*
    Permet de créer des actions avec binding des callbacks du ModalComponent en ajoutant le type d'action de fermeture
  */
  private registerAction(action: string) {
    const actionName = 'on' + this.capitalize(action) + 'Action';
    const responseSubject = 'after' + this.capitalize(action);

    this['on' + this.capitalize(action)] = () => {
      this.bodyComponentInstance.Response[responseSubject].subscribe((result: any) => {
        const response = {
          action: action,
          result: result,
        };
        this.dialogRef.close(response);
      });
      this[actionName].next();
    }
  }

  private capitalize(string: string): string {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  /**
   * S'assure de detruire le ModalComponent lors de la destruction du ModalContainer
   */
  ngOnDestroy() {
    if (this.bodyComponentRef) {
      //this.bodyComponentRef.destroy();
    }
  }
}
