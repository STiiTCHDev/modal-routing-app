import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { ModalConfig, ModalOptions, ModalActions } from './modal-config';
import { ModalResponse } from './modal-response';
import { InjectorService } from '@shared/services/injector.service';

export interface ModalComponent {
  [key: string]: any;
  Modal: ModalConfig;
  ModalOptions?: ModalOptions;
  ModalActions?: ModalActions;

  onCloseAction: Subject<any>;
  onSubmitAction: Subject<any>;
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements ModalComponent, OnInit {

  /* Objets Modal et Response, pour intéragir avec la modal */
  public Modal: ModalConfig = new ModalConfig();
  public Response: ModalResponse = new ModalResponse();

  /* Bindings d'actions & d'events */
  @Input() onCloseAction!: Subject<any>;
  @Input() onSubmitAction!: Subject<any>;
  public onRefresh: Subject<any> = new Subject();

  /* Services préchargés */
  protected formBuilder!: FormBuilder;

  constructor() {
    /* On charge les services avec l'InjectorService pour éviter de devoir passer par le constructor & le super */
    this.formBuilder = InjectorService.getService(FormBuilder);
  }

  ngOnInit() {
    /* On récupère les configs de la modal depuis les helpers enfant */
    if (this.ModalOptions) {
      this.Modal.options = this.ModalOptions;
    }

    if (this.ModalActions) {
      this.Modal.actions = this.ModalActions;
    }

    /* On bind les actions de l'enfant */
    this.bindAction('close');
    this.bindAction('submit');
  }

  /**
   * Bind l'action de l'enfant à l'observable du container
   */
  private bindAction(action: string): void {
    const onAction = 'on' + this.capitalize(action);
    const actionName = onAction + 'Action';

    this[actionName].subscribe(() => {
      if (this[onAction]) {
        this[onAction]();
      }
    });
  }

  private capitalize(string: string): string {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  /**
   * Permet de rafraichir l'enfant et le container via un event
   */
  protected refresh(): void {
    if (this.cdRef) {
      this.cdRef.detectChanges();
    }
    this.onRefresh.next();
  }

}
