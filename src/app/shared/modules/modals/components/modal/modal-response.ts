import { Subject } from 'rxjs';

export interface ModalResponse {
  [key: string]: any;
}

export class ModalResponse {

  public afterClose: Subject<any> = new Subject();
  public afterSubmit: Subject<any> = new Subject();

  public close(result: any = null) {
    this.afterClose.next(result);
  }

  public submit(result: any = null) {
    this.afterSubmit.next(result);
  }
}
