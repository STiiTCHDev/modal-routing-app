
export interface ModalConfig {
  options: ModalOptions;
  actions: ModalActions;
  data?: any;
}

export interface ModalOptions {
  title?: string;
  icon?: string;
}

export interface ModalActions {
  [key: string]: ModalAction
  close: ModalAction;
  submit: ModalAction;
}

export interface ModalAction {
  text: string;
  icon: string;
  confirm: boolean;
}

export class ModalConfig {

  public options: ModalOptions = {
    title: '',
    icon: '',
  }

  public actions: ModalActions = {
    close: {
      text: '',
      icon: '',
      confirm: false,
    },
    submit: {
      text: '',
      icon: '',
      confirm: false,
    }
  }

  public data?: any;

}
