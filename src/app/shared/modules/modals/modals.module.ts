import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../material.module';

import { ModalService } from './services/modal.service';
import { ModalContainerComponent } from './components/modal-container/modal-container.component';
import { ModalComponent } from './components/modal/modal.component';
import { CloseConfirmationModalComponent } from './modals/close-confirmation-modal/close-confirmation-modal.component';
import { DeleteConfirmationModalComponent } from './modals/delete-confirmation-modal/delete-confirmation-modal.component';
import { SharedModule } from '../../shared.module';


@NgModule({
  declarations: [
    ModalContainerComponent,
    ModalComponent,
    CloseConfirmationModalComponent,
    DeleteConfirmationModalComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    MaterialModule,
  ],
  providers: [
    ModalService,
  ],
  entryComponents: [
    ModalContainerComponent,
  ],
  exports: [
    ModalComponent,
  ]
})
export class ModalsModule { }
