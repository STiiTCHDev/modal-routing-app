import { Component, OnInit } from '@angular/core';
import { ModalComponent } from '../../components/modal/modal.component';
import { ModalOptions } from '../../components/modal/modal-config';

@Component({
  selector: 'app-close-confirmation-modal',
  templateUrl: './close-confirmation-modal.component.html',
  styleUrls: ['./close-confirmation-modal.component.scss']
})
export class CloseConfirmationModalComponent extends ModalComponent implements OnInit {

  public ModalOptions: ModalOptions = {
    title: 'Confirmer la fermeture',
  }

  constructor() {
    super();

    this.Modal.actions.submit.text = 'Oui';
  }

  ngOnInit() {
    super.ngOnInit();
    this.refresh();
  }

  onClose() {
    this.Response.close();
  }

  onSubmit() {
    this.Response.submit(true);
  }

}
