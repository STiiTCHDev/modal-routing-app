import { Subject } from 'rxjs';

export class ModalParentCallbacks {

  public onClose: Subject<any> = new Subject();
  public onSubmit: Subject<any> = new Subject();

}
