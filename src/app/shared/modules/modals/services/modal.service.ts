import { ComponentType } from '@angular/cdk/portal';
import { Injectable, ComponentRef, Component, ComponentFactoryResolver, Injector, Type } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalContainerComponent } from '../components/modal-container/modal-container.component';
import { ModalParentCallbacks } from '../models/modal-parent-callback';
import { CloseConfirmationModalComponent } from '../modals/close-confirmation-modal/close-confirmation-modal.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(
    private dialog: MatDialog,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector) { }

    /**
     * Ouvre un component dans le body d'un ModalContainerComponent
     * @param component Component a ouvrir
     * @param callbacks Callbacks du component parent
     * @returns Un objet ModalParentCallbacks pour se greffer action actions depuis le composant appelant.
     */
  public openModal(component: ComponentType<Component> | any, options: MatDialogConfig): ModalParentCallbacks {
    const componentRef = this.getComponentRef(component);

    /* Ouverture du ModalContainerComponent en lui passant un ComponentRef du component appelé et son ComponentType
       ainsi que les data à passer au ModalContainer */
    const dialogRef = this.dialog.open(ModalContainerComponent, {
      data: {
        componentRef: componentRef,
        type: componentRef.componentType,
        data: options.data,
      },
      width: options.width,
      height: options.height,
    });

    /* Gestion de la fermeture du ModalContainer avec binding des callbacks pour le component parent */
    const parentCallbacks = new ModalParentCallbacks();

    dialogRef.afterClosed().subscribe(response => {
      if (response?.action === 'close') {
        parentCallbacks.onClose.next(response.result);
      }
      if (response?.action === 'submit') {
        parentCallbacks.onSubmit.next(response.result);
      }
    });

    return parentCallbacks;
  }

  public openConfirmationModal(action: string, options: MatDialogConfig): ModalParentCallbacks {
    const confirmationModals: any = {
      close: CloseConfirmationModalComponent,
    }

    const modal = this.openModal(confirmationModals[action], options);

    return modal;
  }

  /**
   * Permet d'obtenir l'objet ComponentRef d'un component à partir d'un type de component (sa class)
   */
  private getComponentRef(component: Type<Component>): ComponentRef<Component> {
    const factory = this.componentFactoryResolver.resolveComponentFactory(component);
    const componentRef = factory.create(this.injector);

    return componentRef;
  }

}
