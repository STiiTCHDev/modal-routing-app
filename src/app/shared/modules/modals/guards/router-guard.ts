import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";

import { ModalService } from '../services/modal.service';
import { UrlService } from '@core/services/url.service';

@Injectable({
  providedIn: 'root'
})
export class RouterGuard implements CanActivate {

  constructor(
    private router: Router,
    private UrlService: UrlService,
    private modalService: ModalService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    console.log('Router Guard');
    console.log(state);

    const component = route?.routeConfig?.component;

    this.modalService.openModal(component, {
      data: {

      }
    });

    return true;
  }
}
