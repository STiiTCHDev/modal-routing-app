import { NgModule } from '@angular/core';
import { MaterialModule } from './modules/material.module';
import { ApiService } from './services/api.service';
import { InjectorService } from './services/injector.service';

@NgModule({
  imports: [
    MaterialModule,
  ],
  exports: [
    MaterialModule,
  ],
  providers: [
    ApiService,
    InjectorService,
  ]
})
export class SharedModule { }
