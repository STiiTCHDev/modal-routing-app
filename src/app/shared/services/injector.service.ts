import {  Injector } from '@angular/core';

export class InjectorService {

  private static _injector: Injector;

  constructor() { }

  public static get injector(): Injector {
    return this._injector;
  }

  public static set injector(injector: Injector) {
    this._injector = injector;
  }

  public static getService(service: any): any {
    return this._injector.get(service);
  }

}
