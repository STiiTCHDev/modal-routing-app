import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//import { RouterGuard } from './core/guards/router-guard';
import { UsersPageComponent } from '@modules/users/users-page.component';
import { AppComponent } from './app.component';
import { UserFormModalComponent } from './modules/users/user-form-modal/user-form-modal.component';
import { RouterGuard } from './shared/modules/modals/guards/router-guard';

const routes: Routes = [
  {
    path: '',
    component: AppComponent
  },
  {
    path: 'users',
    component: UsersPageComponent,
    children: [
      {path: 'create', component: UserFormModalComponent, canActivate: [RouterGuard]},
      {path: 'edit/:id', component: UserFormModalComponent, canActivate: [RouterGuard]},
    ]

  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
