import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { ModalsModule } from '@shared/modules/modals/modals.module';

import { UsersPageComponent } from './users-page.component';
import { UserFormModalComponent } from './user-form-modal/user-form-modal.component';




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    CoreModule,
    SharedModule,
    ModalsModule
  ],
  declarations: [
    UsersPageComponent,
    UserFormModalComponent,
  ],
  providers: [
    FormBuilder
  ]
})
export class UsersModule { }
