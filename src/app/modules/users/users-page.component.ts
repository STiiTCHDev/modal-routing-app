import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from '@shared/services/api.service';
import { UserFormModalComponent } from './user-form-modal/user-form-modal.component';
import { ModalService } from '@shared/modules/modals/services/modal.service';
import { User } from '@shared/interfaces/user.interface';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {

  users: User[] = [];
  displayedColumns: string[] = ['id', 'username', 'name', 'email'];
  dataSource!: MatTableDataSource<User>

  constructor(
    private modal: ModalService,
    private api: ApiService) { }

  ngOnInit() {
    this.api.getUsers().subscribe(result => {
      this.users = result;
      this.dataSource = new MatTableDataSource(this.users);
    });
  }

  openUserFormModal(user: User = {}): void {
    const modal = this.modal.openModal(UserFormModalComponent, {
      data: user,
      width: '400px',
      height: '400px',
    });

    modal.onClose.subscribe(() => {
      console.log('onClose', this.users);
    });

    modal.onSubmit.subscribe((result: any) => {
      console.log('onSubmit', result);
      console.log('onSubmit', this.users);

      result.id = this.users.length + 1;
      this.users.push(result);
      this.dataSource.data = this.users;
    });
  }

}
