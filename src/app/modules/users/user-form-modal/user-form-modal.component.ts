import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ModalComponent } from '@shared/modules/modals/components/modal/modal.component';
import { ModalOptions } from '@app/shared/modules/modals/components/modal/modal-config';
import { CloseConfirmationModalComponent } from '@shared/modules/modals/modals/close-confirmation-modal/close-confirmation-modal.component';
import { User } from '@shared/interfaces/user.interface';
import { FormGroup } from '@angular/forms';
import { ModalService } from '../../../shared/modules/modals/services/modal.service';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { filter, map, take, takeLast } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-form-modal',
  templateUrl: './user-form-modal.component.html',
  styleUrls: ['./user-form-modal.component.scss']
})
export class UserFormModalComponent extends ModalComponent implements OnInit {

  public ModalOptions: ModalOptions = {
    title:  'Créer un User',
    icon: '',
  }

  public user: User = {};
  public form!: FormGroup;

  private state!: Observable<object>;

  constructor(public cdRef: ChangeDetectorRef, private modalService: ModalService, private route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();

    this.state = this.route.paramMap.pipe(map(() => window.history.state));
    this.state.subscribe((result) => {
      console.log('state', result);
    });

    this.form = this.formBuilder.group({
      username: '',
      name: '',
      email: '',
    });

    if (this.Modal.data) {
      this.form.patchValue(this.user);
      this.Modal.options.title = 'Editer un User';
      this.Modal.actions.submit.text = 'Modifier';
      this.user = this.Modal.data.user;
    }

    this.refresh();
  }

  onChanges() {
    this.Modal.actions.close.confirm = true;
  }

  onClose() {
    if (this.Modal.actions.close.confirm) {
      const modal = this.modalService.openConfirmationModal('close', {data: {}});

      modal.onSubmit.subscribe((close: boolean) => {
        if (close) {
          this.Response.close();
        }
      });
    } else {
      this.Response.close();
    }

  }

  onSubmit() {
    this.Response.submit(this.form.value);
  }

}
